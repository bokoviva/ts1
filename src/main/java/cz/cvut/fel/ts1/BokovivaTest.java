package cz.cvut.fel.ts1;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assertions.*;
public class BokovivaTest {
    @Test
    public void testFactorial() {
        Bokoviva bokoviva = new Bokoviva();

        long factorial5 = bokoviva.factorial(5);
        long factorial10 = bokoviva.factorial(10);

        Assertions.assertEquals(120, factorial5);
        Assertions.assertEquals(3628800, factorial10);
    }
}
